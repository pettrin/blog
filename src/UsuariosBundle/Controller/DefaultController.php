<?php

namespace UsuariosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UsuariosBundle\Entity\User;
use UsuariosBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{



    /**
     * @Route("/usuarios", name="usuarios")
     */
    public function usuariosAction()
    {
        var_dump($this->getUser());
        return $this->render('@Usuarios/Default/index.html.twig');
    }

    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            /**
             *
             */
            return $this->render('default/sucesspage.html.twig');
        }

        return $this->render(
            'default/register.html.twig',
            array('form' => $form->createView())
        );
    }
//    /**
//     * @Route("/treinadores", name="treinadores")
//     */
//    public function treinadoresAction(Request $request)
//    {
//        // replace this example code with whatever you need
//        return $this->render('frontal/treinadores.html.twig');
//    }
    /**
     * @Route("/usuarios/login", name="login")
     */
    public function loginAction(Request $request,AuthenticationUtils $authenticationUtils)
    {
//         get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

//         last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('UsuariosBundle:Default:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
}
