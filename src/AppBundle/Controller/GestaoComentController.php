<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Blog;
use AppBundle\Entity\Comentarios;
use AppBundle\Repository\BlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/gestaoBlog")
 */
class GestaoComentController extends Controller
{
    /**
     * @Route("/novoComentarioBlog", name="novoComentarioBlog")
     */


//$builder
//->add('descricao', TextareaType::class)
//->add('salvar', SubmitType::class, array('label' => 'Adicionar Comentario'))
    public function novoComentAction(Request $request)
    {
//            return new Response(dump($request->get('comentario')));
        $comentarios = new Comentarios();
        $content = $request->getContent();
        $url = $request->server->get('HTTP_REFERER');
//        dump($request->server->get('HTTP_REFERER'));die;
        $explode = explode('/', $url);
        $blog = $this->getDoctrine()->getRepository(Blog::class)->find(end($explode));
        /**
         * @var $comentariosPost ComentariosPost
         */

//            //Preencher a Entity Blog
        $comentarios->setBlog($blog);
        $comentarios->setCounteudo($request->get('comentario'));
        $comentarios->setData(new \DateTime());
        $comentarios->setAutor($this->getUser());
        if($comentarios->getAutor() == null){
            return $this->render('error/comentarioerror.html.twig');

        }
        //Armazenar novo Blog

        $em = $this->getDoctrine()->getManager();


//        $repository = $this->getDoctrine()
//            ->getRepository(Product::class);
//
//// createQueryBuilder() automatically selects FROM AppBundle:Product
//// and aliases it to "p"
//        $query = $repository->createQueryBuilder('p')
//            ->where('p.price > :price')
//            ->setParameter('price', '19.99')
//            ->orderBy('p.price', 'ASC')
//            ->getQuery();
//
//        $products = $query->getResult();








//        $query = $em->createQuery(
//            'SELECT p FROM AppBundle:Comentarios p ORDER BY p.data ASC');
//        $comentarios = $query->getResult();

        $em->persist($comentarios);
        $em->flush();
//        header('Content-Type: text/html');
//        echo 'The URI requested is: '.$uri;
        return $this->redirectToRoute('blog', array('id' => $blog->getId()));
//        }
//        Verifica se ta passando dados para o home Index

//         replace this example code with whatever you need
        return $this->render('default/register.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/deleteComent/{id}", name="deleteComent")
     */
    public function comentDeleteAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
//        $blog = $this->getDoctrine()->getRepository('AppBundle:Blog')->find($id);
        $comentarios = $em->getRepository('AppBundle:Comentarios')->find($id);
        $comentarios->getBlog()->getId();
        if ($comentarios->getAutor() == $this->getUser()or $this->getUser() == $comentarios->getBlog()->getAutor()) {
            $em->remove($comentarios);
            $em->flush();
        } else {
            return new Response("Voce nao pode deletar de outro usuario");
        };

//        return $this->redirectToRoute('homepage')
        return $this->redirectToRoute('blog',array('id' =>$comentarios->getBlog()->getId()));
    }


}
