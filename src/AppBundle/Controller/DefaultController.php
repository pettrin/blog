<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Blog;
use AppBundle\Entity\Comentarios;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeAction(Request $request)
    {
        //Pegar os dados do BD Doctrine
        $blogRepository = $this->getDoctrine()->getRepository(Blog::class);
        $blogs = $blogRepository->findByTop(1);
        //Verifica se ta passando dados para o home Index
//        var_dump($blogs);
        // replace this example code with whatever you need
        return $this->render('frontal/index.html.twig',array('blogs'=>$blogs));
    }
    /**
     * @Route("/sobre", name="sobre")
     */
    public function sobreAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('frontal/sobre.html.twig');
    }
    /**
     * @Route("/treinadores", name="treinadores")
     */
    public function treinadoresAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('frontal/treinadores.html.twig');
    }
    /**
     * @Route("/duvidas/{url}", name="duvidas")
     */
    public function duvidasAction(Request $request,$url="todos"){
        // replace this example code with whatever you need
        return $this->render('frontal/duvidas.html.twig',array("url"=>$url));
    }

    /**
     * @Route("/blog/{id}", name="blog")
     */
    public function blogAction(Request $request,$id=null)
    {
        // replace this example code with whatever you need
        if ($id!=null) {
            $blogRepository = $this->getDoctrine()->getRepository(Blog::class);
            $blog = $blogRepository->find($id);
            $comentarioRepository = $this->getDoctrine()->getRepository(Comentarios::class);
            $comentarios = $comentarioRepository->findBy(array('blog'=> $blog));
            return $this->render('frontal/blog.html.twig', array("blog"=>$blog,"comentarios"=>$comentarios));

        }else{
                return $this->redirectToRoute('homepage');
        }
//        if($id!=null){
//
//        }

    }
}




