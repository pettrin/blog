<?php
/**
 * Created by PhpStorm.
 * User: ticket
 * Date: 04/06/18
 * Time: 08:15
 */


namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class BlogAtualizarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)    {

        $builder
            ->add('nome', TextType::class)
            ->add('descricao', CKEditorType::class)
            ->add('top')
            ->add('salvar', SubmitType::class, array('label' => 'Atualizar Blog'))
        ;
    }
}