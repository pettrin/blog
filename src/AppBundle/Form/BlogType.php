<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class BlogType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)    {

        $builder
          ->add('nome', TextType::class)
          ->add('descricao', CKEditorType::class)
          ->add('foto', FileType::class,array('attr' =>array('onchange'=>'onChange(event)')))
          ->add('top')
          ->add('salvar', SubmitType::class, array('label' => 'Novo Blog'))
        ;
    }
}