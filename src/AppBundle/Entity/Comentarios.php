<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Blog;

/**
 * Comentarios
 *
 * @ORM\Table(name="comentarios")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComentariosRepository")
 */
class Comentarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="counteudo", type="string", length=255)
     */
    private $counteudo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     */
    private $data;

    /**
     * @ORM\ManyToOne(targetEntity="Blog")
     * @ORM\JoinColumn(name="blog_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $blog;


    /**
     * @ORM\ManyToOne(targetEntity="UsuariosBundle\Entity\User",  inversedBy="users")
     * @ORM\JoinColumn(name="autor_id", referencedColumnName="id")
     */
    private $autor;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set counteudo
     *
     * @param string $counteudo
     *
     * @return Comentarios
     */
    public function setCounteudo($counteudo)
    {
        $this->counteudo = $counteudo;

        return $this;
    }

    /**
     * Get counteudo
     *
     * @return string
     */
    public function getCounteudo()
    {
        return $this->counteudo;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Comentarios
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    public function getBlog() {
        return $this->blog;
    }

    public function setBlog($blog) {
        $this->blog = $blog;
    }
    public function getAutor() {
        return $this->autor;
    }

    public function setAutor($autor) {
        $this->autor = $autor;
    }
}

