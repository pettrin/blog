var tiposValidos =
[
    'image/jpeg',
    'image/png'
];

function validacaoTipos(file){
    for(var i=0;i<tiposValidos.length;i++){
        if(file.type===tiposValidos[i]){
            return true;
        }
    }
    return false;
}

function onChange(event){
    var file=event.target.files[0];
    if(validacaoTipos(file)){
       var blogMiniatura=document.getElementById('blogThumb');
       blogThumb.src=window.URL.createObjectURL(file);
    }
}
